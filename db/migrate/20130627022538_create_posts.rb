class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.text :content
      t.integer :from_user_id
      t.integer :to_user_id

      t.timestamps
    end

    add_index :posts, :from_user_id
    add_index :posts, :to_user_id
  end
end
