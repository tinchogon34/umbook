class ChangeProfilePictureToUsers < ActiveRecord::Migration
  def change
    remove_column :users, :profile_picture
    add_column :users, :profile_picture_uid, :string
  end
end
