# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(document).ready ->
  $(".simple_form .post_comment").on("ajax:success", (e,data,status,xhr) ->
    $("#post-message").attr("class","alert alert-success")
    $("#post-message").html("Comentario creado exitosamente")
    $("#post_comment_content").val(""))
