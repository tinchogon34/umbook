class FriendshipsController < ApplicationController
  load_and_authorize_resource

  def create
    friendship = Friendship.new(params[:friendship])
    friendship.from_user = current_user
    friendship.status = false
    friendship.save

    respond_to do |format|
      format.html do 
        redirect_to :back, notice: 'Solicitud enviada exitosamente.'
      end
    end
  end

  def update
    friendship = Friendship.find(params[:friendship][:friendship_id])
    friendship.status = true
    friendship.save

    respond_to do |format|
      format.html do 
        redirect_to :back, notice: 'Solicitud aceptada exitosamente.'
      end
    end
  end
end
