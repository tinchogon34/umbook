class UsersController < ApplicationController  
  load_and_authorize_resource

  def edit
  end
  
  def show
    @idUsuario = params[:id]
  end

  def search
    @users = User.find_by_name_or_lastname(params[:q], current_user.id)
  end

  def update
    @user = User.find(params[:id])
    @user.update_attributes(params[:user])
    redirect_to :back
  end

  def destroy
  end
end
