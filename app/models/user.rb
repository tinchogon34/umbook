class User < ActiveRecord::Base
  image_accessor :profile_picture
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable, :lockable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me,
    :name, :lastname, :sex, :born_date, :profile_picture, :retained_profile_picture, :remove_profile_picture
  # attr_accessible :title, :body
  
  has_many :albums
  has_many :photo_comments
  has_many :post_comments
  has_many :sended_posts, foreign_key: 'from_user_id', class_name: 'Post'
  has_many :received_posts, foreign_key: 'to_user_id', class_name: 'Post'
  has_many :sended_invites, foreign_key: 'user1_id', class_name: 'Friendship'
  has_many :received_invites, foreign_key: 'user2_id', class_name: 'Friendship'

  def posts
    Post.where("from_user_id = ? OR to_user_id = ?",id,id).order("updated_at DESC")
  end

  def all_posts
    all_posts = posts
    friends.each do |friend|
      all_posts.concat friend.posts
    end
    all_posts.uniq{|p| p.id }.sort_by{|p| p.updated_at}.reverse
  end

  def friends
    friends = sended_invites.where(status: true).map { |si| si.to_user }
    friends.concat received_invites.where(status: true).map { |ri| ri.from_user }
  end

  def invite_pending?(user)
    received_invites.where(user1_id: user, status: false).exists? || sended_invites.where(user2_id: user, status: false).exists?
  end

  def friend_of?(user)
    received_invites.where(user1_id: user, status: true).exists? || sended_invites.where(user2_id: user, status: true).exists? || (user.id == id)
  end

  def invite_to?(user)
    sended_invites.where(user2_id: user).exists?
  end

  def friendship_id(user)
    ri = received_invites.where(user1_id: user).first
    si = sended_invites.where(user2_id: user).first
    if ri
      friendship_id = ri.id
    elsif si
      friendship_id = si.id
    end
    friendship_id  
  end

  def self.find_by_name_or_lastname(query, id)
    query += "%"
    where("id != ? AND (name ILIKE ? or lastname ILIKE ? OR (name || ' ' ||lastname) ILIKE ?)",id,query,query,query)
  end
end
