class Photo < ActiveRecord::Base
  image_accessor :image

  belongs_to :album
  attr_accessible :description, :album_id, :image, :retained_image, :remove_image
end
