class Friendship < ActiveRecord::Base
  belongs_to :from_user, foreign_key: 'user1_id', class_name: 'User'
  belongs_to :to_user, foreign_key: 'user2_id', class_name: 'User'

  attr_accessible :user2_id

end
