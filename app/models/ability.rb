class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
    
    alias_action :create, :read, :update, :destroy, :to => :crud

    #user ||= User.new
    
    if user
      can [:search], [User]
      can [:show], [User]

      can [:read], Album do |album| # si es album suyo o es amigo
       (album.user == user) || (album.user.friend_of?(user))
      end

      can [:read], Photo do |photo| #si es foto suya o es amigo
        (user.album_ids.include?(photo.album.id)) || (photo.album.user.friend_of?(user))
      end

      can [:read], Post do |post| #si es un post mio o de un amigo
        (post.from_user == user) || (post.to_user == user) || (post.from_user.friend_of?(user)) || (post.to_user.friend_of?(user))
      end

      can [:read], [PostComment] do |post_comment| #si es un comentario mio, de un amigo o en el post de un amigo
        (post_comment.user == user) || (post_comment.user.friend_of?(user)) || (post_comment.post.from_user.friend_of?(user)) || (post_comment.post.to_user.friend_of?(user))
      end

      can [:create], [Album, Photo, Post, PostComment, Friendship]

      can [:update, :destroy], [User], id: user.id
      can [:update, :destroy], [Album, Photo, Post, PostComment], user_id: user.id
      can [:update, :destroy], [Friendship], user2_id: user.id
    end
  end
end
