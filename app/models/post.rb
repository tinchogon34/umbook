class Post < ActiveRecord::Base
  belongs_to :from_user, foreign_key: 'from_user_id', class_name: 'User'
  belongs_to :to_user, foreign_key: 'to_user_id', class_name: 'User'
  has_many :post_comments
  attr_accessible :content, :to_user_id
end
