module UsersHelper
  def full_name(user, link = false)
    full_name = "#{user.name} #{user.lastname}"
    if link
      full_name = link_to(full_name, user)
    end
    full_name
  end

  def user_picture(user, size)
    return user.profile_picture.thumb(size).url if user.profile_picture
    "rails.png"
  end

  def invite_message(user)
  end
end
